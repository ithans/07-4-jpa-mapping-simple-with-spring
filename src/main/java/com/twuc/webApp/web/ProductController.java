package com.twuc.webApp.web;

import com.twuc.webApp.contract.CreateProductRequest;
import com.twuc.webApp.domain.Product;
import com.twuc.webApp.domain.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class ProductController {
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private EntityManager em;
    @PostMapping("/products")
    public ResponseEntity get(@RequestBody @Valid CreateProductRequest request){
        Product product = productRepository.save(new Product(request.getName(), request.getPrice(), request.getUnit()));
        productRepository.flush();

        return ResponseEntity
                .status(HttpStatus.CREATED)
                .header("location","http://localhost/api/products/"+product.getId())
                .build();
    }

    @GetMapping("/products/{productId}")
    public ResponseEntity<Product> getEntity(@PathVariable Long productId){
        Optional<Product> product = productRepository.findById(productId);
        if (!product.isPresent()){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        return ResponseEntity
                .ok()
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .body(product.get());
    }

    @ExceptionHandler
    public ResponseEntity<String> runtimeHandel(RuntimeException exception) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Something wrong with the argument");
    }

}